from django.conf.urls import include
from django.contrib import admin
from django.urls import path

from apps.chat.urls import urlpatterns as chat_urls


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('apps.auth.urls')),
]


urlpatterns += chat_urls
