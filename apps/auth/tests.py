from django.test import TestCase
from django.contrib.auth.models import User
from django.test import Client


class ChatTestCase(TestCase):
    def setUp(self):
        self.username = "test_username"
        self.user_password = "Testpassword42"
        self.user = User.objects.create(username=self.username)
        self.user.set_password(self.user_password)
        self.user.save()
        self.client = Client()

    def test_sign_up_success(self):
        test_username = "test_username_2"
        self.assertFalse(User.objects.filter(username=test_username).exists())
        response = self.client.post("/sign-up/", {"login": test_username, "password": self.user_password,
                                                  "confirm_password": self.user_password}, follow=True)
        self.assertEqual(response.status_code, 200)
        user = User.objects.get(username=test_username)
        self.assertTrue(user.is_authenticated)

    def test_sign_up_fail(self):
        test_username = "test_username_2"
        self.assertFalse(User.objects.filter(username=test_username).exists())
        response = self.client.post("/sign-up/", {"login": "new_user", "password": self.user_password,
                                                  "confirm_password": "wrong_password"}, follow=True)
        self.assertContains(response, "Passwords do not match")

    def test_sign_in_success(self):
        self.client.logout()
        response = self.client.post("/sign-in/", {"login": self.user.username, "password": self.user_password},
                                    follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.user.is_authenticated)

    def test_sign_in_wrong_credentials(self):
        self.client.logout()
        response = self.client.post("/sign-in/", {"login": self.user.username, "password": "wrong+password"},
                                    follow=True)
        self.assertContains(response, "wrong credentials")

    def test_sign_out_success(self):
        self.client.post("/sign-in/", {"login": self.username, "password": self.user_password})
        self.assertTrue(self.user.is_authenticated)
        response = self.client.get("/sign-out/", follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, """<p>You need <a href="/sign-in">Sign-in</a>""""")
