from django.views.generic import FormView
from django.contrib.auth.models import User
from django.contrib.auth import login, logout
from django.http import HttpResponseRedirect

from .forms import SignUpForm, SignInForm


class SignUpView(FormView):
    form_class = SignUpForm
    template_name = "chat/sign-up.html"
    success_url = "/"

    def form_valid(self, form):
        data = form.data
        username = data.get("login")
        password = data.get("password")
        user = User.objects.create(username=username, password=password)
        login(self.request, user)
        return HttpResponseRedirect(self.get_success_url())


class SignInView(FormView):
    form_class = SignInForm
    template_name = "chat/sign-in.html"
    success_url = "/"

    def form_valid(self, form):
        data = form.data
        username = data.get("login")
        user = User.objects.get(username=username)
        login(self.request, user)
        return HttpResponseRedirect(self.get_success_url())


def logout_view(request):
    logout(request)
    return HttpResponseRedirect("/")
