# chat/urls.py
from django.urls import path

from apps.chat import views

urlpatterns = [
    path('', views.index, name='index')
]