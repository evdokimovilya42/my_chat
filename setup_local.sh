#!/bin/bash
touch log/chat.log
docker-compose -f docker-compose.yml build
docker-compose run web /usr/local/bin/python manage.py migrate
docker-compose run web /usr/local/bin/python manage.py test
docker-compose -f docker-compose.yml up -d
